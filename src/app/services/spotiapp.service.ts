import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotiappService {

  constructor( private http:HttpClient ) { }

  getQuery( query: string){
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCaRoYfZFDnCincia08F94gnwzQR6aV1-Zn-rUmWoqRzbXN8JztT8_cHOshqwUKgIgd9-oj6pO4vewQ1nI'
    })

    return this.http.get(url,{headers});
  }

  getNewReleases(){

    return this.getQuery('browse/new-releases?limit=20')
    .pipe( map(data => data['albums'].items ))
    
    // return this.http.get("https://api.spotify.com/v1/browse/new-releases", { headers })
    // .pipe( map(data => {
    //   return data['albums'].items
    // }))
    // return this.http.get("https://api.spotify.com/v1/browse/new-releases", { headers })
    // .subscribe(data => {
    //   console.log(data);
    // })
  }

  getArtistas(termino: string){
    return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
    .pipe( map(data => data['artists'].items ))

    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=15`, { headers })
    // .pipe( map(data => data['artists'].items ))
  }

  getArtista(id: string){
    return this.getQuery(`artists/${id}`)
  }

  getTopTracks(id: string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
    .pipe( map(data => data['tracks'] ))
  }
}
