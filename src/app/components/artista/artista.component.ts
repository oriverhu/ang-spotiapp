import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotiappService } from '../../services/spotiapp.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
  ]
})
export class ArtistaComponent implements OnInit {

  artista:any={};
  topTracks:any[]=[];
  loading: boolean;

  constructor( private router: ActivatedRoute, private spotify: SpotiappService ) { 
    this.loading=true;
    this.router.params.subscribe(params =>{
       this.getArtista(params['id'])
       this.getTopTracks(params['id'])
      })
  }

  ngOnInit(): void {
  }

  getArtista(id){    
    this.spotify.getArtista(id)
    .subscribe((data:any) => {
      this.artista=data
      this.loading=false;
    })
  }

  getTopTracks(id){    
    this.spotify.getTopTracks(id)
    .subscribe((data:any) => {
      console.log(data)
     this.topTracks = data
    })
  }

}
