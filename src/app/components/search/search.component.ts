import { Component, OnInit } from '@angular/core';
import { SpotiappService } from '../../services/spotiapp.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent implements OnInit {

  artistas:any[] = [];
  loading:boolean;

  constructor( private spotify: SpotiappService ) { }

  ngOnInit(): void {
  }

  buscar(termino: string){
    if(termino){
      this.loading = true
      this.spotify.getArtistas(termino)
      .subscribe((data:any) => {
      //  this.artistas = data.artists.items
        this.artistas = data
        this.loading = false
      })
    }else{
      this.artistas = []
    }
  }

}
