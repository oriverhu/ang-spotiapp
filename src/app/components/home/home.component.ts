import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { SpotiappService } from '../../services/spotiapp.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  // paises:any[] = [];
  nuevasCanciones:any[] = [];
  loading:boolean;
  error:string=null;

    // constructor( private http: HttpClient ) { 
    // this.http.get("https://restcountries.eu/rest/v2/lang/es")
    // .subscribe( (data:any)=> {
    //   this.paises = data;
    //   console.log(data)
    // }) 

    constructor( private spotify: SpotiappService ){
      this.loading = true
      this.spotify.getNewReleases()
      .subscribe((data:any) => {
      //  this.nuevasCanciones = data.albums.items
        this.nuevasCanciones = data
        this.loading = false
      },(e)=>{
        this.error = e.error.error.message
      })
    }


    ngOnInit(): void {
    }

}
